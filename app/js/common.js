document.addEventListener("DOMContentLoaded", function() {
  // $(function() {

//   $(".gallery-item-popup").magnificPopup({
//     type: "image",
//     gallery: {
//       enabled: true
//     }
//   });
// });

  // mobile menu
  var menu__button = document.querySelector('.menu__button');
  var menu = document.querySelector('.menu__body');
  var close = document.querySelector('.menu__header .btn-close');
  var overlay = document.querySelector('.menu__overlay');


  function showMenu () {
    menu__button.setAttribute('hidden', '');
    menu.removeAttribute('hidden');
    overlay.removeAttribute('hidden');
    document.body.style.overflowY = 'hidden';
    document.documentElement.style.overflowY = 'hidden';
  };

  function hideMenu () {
    menu.setAttribute('hidden', '');
    overlay.setAttribute('hidden', '');
    menu__button.removeAttribute('hidden');
    document.body.style.overflowY = 'auto';
    document.documentElement.style.overflowY = 'auto';
  };

  menu__button.addEventListener('click', showMenu);
  close.addEventListener('click', hideMenu);
  overlay.addEventListener('click', hideMenu);

  // mobile menu end



  var mySiema = new Siema({
    selector: ".siema",
    duration: 200,
    easing: "ease-out",
    perPage: {
      100: 2,
      768: 2,
      1024: 5
    },
    startIndex: 0,
    draggable: true,
    multipleDrag: true,
    threshold: 20,
    loop: true,
    rtl: false
  });
  
  document
    .querySelector(".next-container")
    .addEventListener("click", function() {
      mySiema.next();
    });
  
  var mySiemaReviews = new Siema({
    selector: ".siema-reviews",
    loop: true,
    duration: 200,
    easing: "ease-out",
    perPage: {
      100: 2,
      768: 2,
      1024: 5
    },
    startIndex: 0,
    draggable: true,
    multipleDrag: true,
    threshold: 20,
    rtl: false
  });
  document
    .querySelector(".next-container-reviews")
    .addEventListener("click", function() {
      mySiemaReviews.next();
    });
});

// Custom JS
