
Vue.use(VueObserveVisibility);
Vue.directive("observe-visibility", VueObserveVisibility.ObserveVisibility);

Vue.component("limg", {
  props: ["src"],
  data: function() {
	  return {
		imgSrc: '/img/1x1.gif'
	  }
  },
  template: "#imgTemplate",
  methods: {
    handleView: function(isVisible) {
      if (!isVisible) return
      this.imgSrc = this.src || '/img/icons/pill.svg'
    }
  }
});

Vue.component("collapse", {
  template: "#collapse",
  props: ['name'],
  data: function() {
    return {
      isOpened: true
    }
  },
  methods: {
    opened: function() {
      if (this.isOpened) {
        this.close(close)
      } else {
        this.open()
      }
      this.isOpened = !this.isOpened
    },
    open: function () {
      var maxHeight = this.$el.querySelector('.u-collapse-body').scrollHeight
      this.$el.querySelector('.u-collapse-body').style.maxHeight = maxHeight + 'px'
    },
    close: function () {
      this.$el.querySelector('.u-collapse-body').style.maxHeight = null
    }
  },
  mounted: function() {
    this.open()
  },
});

var app = new Vue({
  el: "#app",
  data: function() {
    return {
      postUrl: "https://uprint.pro/post.php",
  
      popupError: {
        show: false,
        text: 'Помилка'
      },
      spiner: false,
      callbackForm: false,
      popupSubmited: false,
      popupOrderCatalog: false,
      popupPhotoSubmited: false,
      name: "",
      phone: "",
      servise: "",
      size: "",
      error: false,
      image: null,
      uploadedImg: {},
      popupOrder: false,

      order: {},

      activeService: {
        index: 1,
        img: null
      }
    };
  },
  methods: {
    catalogBuy: function(name, url) {
      var vm = this;
      vm.servise = name;
      vm.uploadedImg.url = url
      vm.popupOrderCatalog = true;
    },

    setActiveService: function(index, img) {
      this.activeService = {
        index: index,
        img: img
      }
    },
    selectService: function(x) {
      var vm = this;
      vm.servise = x;
      vm.popupOrder = true;
    },
    selectSize: function(x) {
      var vm = this;
      vm.size = x;
      vm.popupOrder = true;
    },
    sendCallBack: function() {
      var vm = this;
      if (vm.phone.length < 18) {
        vm.error = true;
        setTimeout(function() {
          vm.error = false;
        }, 3000);
      } else {
        vm.spiner = true;

        var data = new FormData();

        data.append('type', 'callback')
        data.append('name', vm.name)
        data.append('phone', vm.phone)

        window.axios
          .post(vm.postUrl, data)
          .then(function(response) {
            vm.spiner = false;
            vm.callbackForm = false;

            vm.popupSubmited = true;
          })
          .catch(function(error) {
            vm.spiner = false;
          });
      }
    },
    sendOrder: function(place) {
      var vm = this;
      if (vm.phone.length < 18) {
        vm.error = true;
        setTimeout(function() {
          vm.error = false;
        }, 3000);
      } else {
        vm.spiner = true;

        var data = new FormData();

        data.append('type', 'order')
        data.append('name', vm.name)
        data.append('phone', vm.phone)
        data.append('place', place)

        if (this.servise.length > 2) {
          data.append('service', this.servise)
        }

        if (this.size.length > 2) {
          data.append('size', this.size)
        }
        
        if (this.uploadedImg.url) {
          data.append('photo', "https://uprint.pro/upload" + this.uploadedImg.url)
        }

        window.axios.post(vm.postUrl, data);

        vm.popupOrder = false;
        vm.popupOrderCatalog = false
        vm.spiner = false;

        vm.popupSubmited = true;

        fbq('track', 'Purchase');
      }
    },
    uploadPhoto: function(e) {
      var vm = this

      vm.spiner = true;

      var data = new FormData();
      var files = e.target.files || e.dataTransfer.files;

      data.append('file', files[0]);

      window.axios
        .post("/upload/image.php", data)
        .then(function(response) {
          vm.spiner = false;
          vm.uploadedImg = response.data;
          vm.popupPhotoSubmited = true;

          setTimeout(function() {
            vm.popupPhotoSubmited = false;
          }, 3000);
        })
        .catch(function(err) {
          vm.popupError = {
            show: true,
            text: 'Помилка завантаження фото. Ви зможете відправити його пізніше.'
          }
          vm.spiner = false;
        });
    }
  }
});
