<?php
// ©EDGE 2019

class Response {
    protected $status;
    protected $response;

    public function __construct($response, $status = 200) {
        $this->response = $response;
        $this->status = $status;
    }

    public function getResponse() {
        return $this->response;
    }

    public function getStatus() {
        return $this->status;
    }

    public function __toString() {
        if (is_array($this->response)) {
            return json_encode($this->response);
        } else {
            return $this->response;
        }
    }
}

class ValidationErrorResponse extends Response {
    public function __construct($error) {
        $this->response = ['error' => $error];
        $this->status = 422;
    }
}

function app() {
    $file = $_FILES['file'] ?? null;
    if (!$file) {
        return new ValidationErrorResponse(['file' => 'required']);
    } elseif ($file['size'] < 100 * 1000) {
        return new ValidationErrorResponse(['file' => 'Фото плохого качества']);
    } elseif ($file['size'] > 1024 * 5000) {
        return new ValidationErrorResponse(['file' => 'Файл слишком большой, вы сможете отправить его после создания заявки']);
    } elseif (file_exists(__DIR__ . "/storage/{$file['name']}")) {
        return new Response(['file' => 'Этот файл уже загружен', 'url' => "/storage/{$file['name']}"]);
    } elseif (!preg_match('/^image\/.+$/', mime_content_type($file['tmp_name']))) {
        return new ValidationErrorResponse(['file' => 'accepts only images']);
    }

    move_uploaded_file($file['tmp_name'], __DIR__ . "/storage/{$file['name']}");

    return new Response(['url' => "/storage/{$file['name']}"]);

};


$response = app();

if ($response instanceof Response) {
    http_response_code($response->getStatus());
    die($response);
} else {
    http_response_code(500);
    die();
}
